package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Font;
import javax.swing.JLabel;

import gui.About;
import gui.Game;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelSelect extends JPanel {
	private ButtonEventHandler eventHandler;
	static JPanel panel;
	private Game game = new Game();
	public LevelSelect(){
		setBounds(100, 100, 795, 595);
		setLayout(null);
		setVisible(true);
		
		panel=new JPanel();
		panel.setBackground(new Color(244, 164, 96));
		panel.setBounds(0, 0, 795, 595);
		eventHandler=new ButtonEventHandler();
		panel.setLayout(null);
		panel.setVisible(true);
		add(panel);
		
		
		JButton l1 = new JButton("Easy");
		l1.setBackground(new Color(255, 255, 255));
		l1.setFont(new Font("Dialog", Font.BOLD, 17));
		l1.setBounds(342, 168, 111, 58);
		panel.add(l1);
		l1.addActionListener(eventHandler);
		
		JButton l2 = new JButton("Medium");
		l2.setBackground(new Color(255, 255, 255));
		l2.setFont(new Font("Dialog", Font.BOLD, 17));
		l2.setBounds(342, 238, 111, 58);
		panel.add(l2);
		l2.addActionListener(eventHandler);
		
		JButton l3 = new JButton("Hard");
		l3.setBackground(new Color(255, 255, 255));
		l3.setFont(new Font("Dialog", Font.BOLD, 17));
		l3.setBounds(342, 308, 111, 58);
		panel.add(l3);
		l3.addActionListener(eventHandler);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBackground(new Color(255, 255, 255));
		btnExit.setFont(new Font("Dialog", Font.BOLD, 17));
		btnExit.setBounds(12, 519, 98, 64);
		panel.add(btnExit);
		btnExit.addActionListener(eventHandler);
		
		JButton btnAbout = new JButton("About");
		btnAbout.setBackground(new Color(255, 255, 255));
		btnAbout.setFont(new Font("Dialog", Font.BOLD, 17));
		btnAbout.setBounds(685, 519, 98, 64);
		panel.add(btnAbout);
		btnAbout.addActionListener(eventHandler);
		
		JLabel lblPalsSpellingGame = new JLabel("PALS Spelling Game");
		lblPalsSpellingGame.setBackground(new Color(255, 255, 255));
		lblPalsSpellingGame.setForeground(new Color(0, 0, 0));
		lblPalsSpellingGame.setFont(new Font("Tahoma", Font.BOLD, 42));
		lblPalsSpellingGame.setBounds(186, 47, 423, 90);
		panel.add(lblPalsSpellingGame);
	}
	private class ButtonEventHandler implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
        	if("Exit".equals(e.getActionCommand())){
                System.exit(0);
        	}
        	if("About".equals(e.getActionCommand())){
        		About frame;
        		frame = new About();
                frame.setVisible(true);
                        
        	}
        	if("Easy".equals(e.getActionCommand())){
        		
        		add(game);
        		game.refresh(1);
        		game.setVisible(true);
        		panel.setVisible(false);
        		System.out.println(game.level);
        	}
        	if("Medium".equals(e.getActionCommand())){
        		
        		add(game);
        		game.refresh(2);
        		game.setVisible(true);
        		panel.setVisible(false);
        	}
        	if("Hard".equals(e.getActionCommand())){
        		
        		add(game);
        		game.refresh(3);
        		game.setVisible(true);
        		panel.setVisible(false);
        	}
        	if("Back To Levels".equals(e.getActionCommand())){
                game.setVisible(false);
                panel.setVisible(true);
        	}
        }
    }
}
