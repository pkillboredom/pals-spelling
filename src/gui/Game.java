//thanks to dan for being a baddie

package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Color;

@SuppressWarnings("serial")
public class Game extends JPanel {
	private ButtonEventHandler eventHandler;
	private String activeString,realAnswer,activePhrase,activeImage;
	private String winOrLose;
	private JTextField input;
	private JLabel lbTryAgain,lbAnswer,lbPhrase;
	private JButton btnNext;
	private JButton btnRestart;
	int level,correct,wrong;
	private ArrayList<String> wordList=readWordFile(level);
	private JLabel lbCorrect,lblImage;
	private BufferedImage img = null;
	private String imgPath;
	public Game(){
		setBackground(new Color(244, 164, 96));
		eventHandler=new ButtonEventHandler();
		setBounds(0, 0, 795, 595);
		setVisible(true);
		//System.out.println(level);
		
		lbPhrase = new JLabel(activePhrase);
		lbPhrase.setBounds(12, 12, 760, 33);
		lbPhrase.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbPhrase.setHorizontalAlignment(SwingConstants.CENTER);
		
		lbAnswer = new JLabel("The Correct Spelling is " + realAnswer);
		lbAnswer.setBounds(17, 45, 760, 33);
		lbAnswer.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbAnswer.setHorizontalAlignment(SwingConstants.CENTER);
		lbAnswer.setVisible(false);
		
		
		input = new JTextField();
		input.setBounds(169, 361, 456, 26);
		input.setFont(new Font("Tahoma", Font.PLAIN, 18));
		input.setColumns(20);
		
		JButton btnBackToLevels = new JButton("Back to Levels");
		btnBackToLevels.setBackground(Color.WHITE);
		btnBackToLevels.setBounds(17, 463, 126, 120);
		btnBackToLevels.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBackToLevels.addActionListener(eventHandler);
		btnBackToLevels.setIcon(new ImageIcon("img/back.png"));
		btnBackToLevels.setHorizontalAlignment(SwingConstants.CENTER);
		btnBackToLevels.setVerticalAlignment(SwingConstants.CENTER);
		
		JButton btnShowSpelling = new JButton("Show Spelling");
		btnShowSpelling.setBackground(Color.WHITE);
		btnShowSpelling.setBounds(382, 463, 126, 120);
		btnShowSpelling.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnShowSpelling.addActionListener(eventHandler);
		btnShowSpelling.setIcon(new ImageIcon("img/show.png"));
		
		JButton btnCheckSpelling = new JButton("Check Spelling");
		btnCheckSpelling.setBackground(Color.WHITE);
		btnCheckSpelling.setBounds(514, 463, 126, 120);
		btnCheckSpelling.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCheckSpelling.addActionListener(eventHandler);
		btnCheckSpelling.setIcon(new ImageIcon("img/check.png"));
		
		btnNext = new JButton("Next");
		btnNext.setEnabled(false);
		btnNext.setBounds(646, 463, 126, 120);
		btnNext.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNext.addActionListener(eventHandler);
		btnNext.setIcon(new ImageIcon("img/next.png"));
		setLayout(null);
		add(lbPhrase);
		add(lbAnswer);
		add(btnBackToLevels);
		add(btnShowSpelling);
		add(btnCheckSpelling);
		
		btnRestart = new JButton("Restart");
		btnRestart.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnRestart.setVisible(false);
		btnRestart.setEnabled(false);
		btnRestart.setBounds(646, 463, 126, 120);
		btnRestart.addActionListener(eventHandler);
		btnRestart.setIcon(new ImageIcon("img/restart.png"));
		add(btnRestart);
		add(btnNext);
		add(input);
		
		lbTryAgain = new JLabel();
		lbTryAgain.setText(winOrLose);
		lbTryAgain.setVisible(false);
		lbTryAgain.setFont(new Font("Dialog", Font.BOLD, 15));
		lbTryAgain.setBounds(294, 399, 206, 26);
		lbTryAgain.setHorizontalAlignment(SwingConstants.CENTER);
		add(lbTryAgain);
		
		lbCorrect = new JLabel("You got " + correct + " correct and " + wrong + " wrong. " + getPercentCorrect() + "% of your answers have been correct.");
		lbCorrect.setBounds(188, 436, 418, 14);
		lbCorrect.setVisible(false);
		lbCorrect.setHorizontalAlignment(SwingConstants.CENTER);
		add(lbCorrect);
		
		lblImage = new JLabel("");
		lblImage.setBounds(169, 90, 456, 259);
		add(lblImage);
		lblImage.setHorizontalAlignment(SwingConstants.CENTER);
		
		
	}
	private class ButtonEventHandler implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
         	if("Check Spelling".equals(e.getActionCommand())){
         		//System.out.println("debug button");
         		if(checkSpelling()==true){
         			if(gotEm()==false){
         				winOrLose="You got it!";
         				btnNext.setEnabled(true);
         			}
         			if(gotEm()==true){
         				winOrLose="You got them all!";
         				lbTryAgain.setText(winOrLose);
         				lbTryAgain.setVisible(true);
         				btnNext.setVisible(false);
         				btnRestart.setVisible(true);
         				btnRestart.setEnabled(true);
         				JOptionPane.showMessageDialog(getParent(), "You got all the words on this level!");
         			}
         			//System.out.println("DEBUG WINLINE: " + winOrLose);
         			lbTryAgain.setText(winOrLose);
         			lbTryAgain.setVisible(true);
         			lbAnswer.setText("The Correct Spelling is " + realAnswer);
         			lbAnswer.setVisible(true);
         			//System.out.println("debug true");
         		}
         		else{
         			winOrLose="Sorry, Try Again.";
         			lbTryAgain.setText(winOrLose);
         			lbTryAgain.setVisible(true);
         			lbCorrect.setVisible(true);
         			//System.out.println("debug false");
         		}
         	}
         	if("Show Spelling".equals(e.getActionCommand())){
         		lbAnswer.setVisible(true);
         		lbAnswer.setText("The Correct Spelling is " + realAnswer);
         	}
         	if("Next".equals(e.getActionCommand())){
         		newWord();
         		winOrLose=null;
         		lbTryAgain.setVisible(false);
         		lbAnswer.setVisible(false);
         		btnNext.setEnabled(false);
         		lbPhrase.setText(activePhrase);
         		input.setText("");
         		getImage();
        		lblImage.setIcon(new ImageIcon(img));
         	}
         	if("Restart".equals(e.getActionCommand())){
         		restart();
         	}
         	if("Back to Levels".equals(e.getActionCommand())){
         		//System.out.println("debug back");
         		setVisible(false);
         		LevelSelect.panel.setVisible(true);
         	}
        }
    }
	private void newWord(){
		if(gotEm()==true){}
		else{
			Random gen = new Random();
			int current = gen.nextInt(wordList.size());
			activeString = wordList.get(current);
			wordList.remove(current);
			String wordHold = null;
			String errType = null;
			String errMsg = null;
			try{
				wordHold=activeString.substring(2);//grabs the word on
				realAnswer=wordHold.substring(0,wordHold.indexOf("^"));//limits it to the word
			}
			catch(StringIndexOutOfBoundsException e){
				errType="word";
				errMsg="Parsing failed on line \""+activeString+"\" while parsing the " + errType + ".";
				JOptionPane.showMessageDialog(getParent(), errMsg, "Critical Error", JOptionPane.ERROR_MESSAGE);
				newWord();
			}
			//System.out.println(realAnswer);
			String phraseHold = null;
			try{
				phraseHold=wordHold.substring(wordHold.indexOf("^")+1);
				activePhrase=phraseHold.substring(0,phraseHold.indexOf("^"));
			}
			catch(StringIndexOutOfBoundsException e){
				errType="phrase";
				errMsg="Parsing failed on line \""+activeString+"\" while parsing the " + errType + ".";
				JOptionPane.showMessageDialog(getParent(), errMsg, "Critical Error", JOptionPane.ERROR_MESSAGE);
				newWord();
			}
			try{
				activeImage=phraseHold.substring(phraseHold.indexOf("^")+1);	
			}
			catch(StringIndexOutOfBoundsException e){
				errType="image";
				errMsg="Parsing failed on line \""+activeString+"\" while parsing the " + errType + ".";
				JOptionPane.showMessageDialog(getParent(), errMsg, "Critical Error", JOptionPane.ERROR_MESSAGE);
				newWord();
			}
					
		}
	}
	private boolean checkSpelling(){
		//System.out.println("DEBUG input: " + input.getText());
		if((realAnswer.toLowerCase()).equals((input.getText()).toLowerCase())==true){
			correct++;
			lbCorrect.setText("You got " + correct + " correct and " + wrong + " wrong. " + getPercentCorrect() + "% of your answers have been correct.");
			lbCorrect.setVisible(true);
			return true;
		}
		else{
			wrong++;
			lbCorrect.setText("You got " + correct + " correct and " + wrong + " wrong. " + getPercentCorrect() + "% of your answers have been correct.");
			return false;		
		}
	}
	private boolean gotEm(){
		if(wordList.size()==0)return true;
		else return false;
	}
	public void setLevel(int levelIn){
		level=levelIn;
	}
	private ArrayList<String> readWordFile(int level){
		//get the words that match the level
		Scanner wordfile = null;
		try {
			wordfile = new Scanner(new File ("words.txt"));
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		ArrayList<String> wordList = new ArrayList<String>();
		while(wordfile.hasNextLine()){
			wordList.add(wordfile.nextLine());
		}
		wordfile.close();
		char cLevel=(Integer.toString(level)).charAt(0);
		//System.out.println("DEBUG cLevel:" + cLevel);
		for(int x=0;x<wordList.size();x++){
			//System.out.println("DEBUG: " + x + " " + wordList.get(x));
			if((wordList.get(x)).charAt(0)!=cLevel){
				wordList.remove(x);
				x=x-1;
			}
		}
		//System.out.println("Debug wordList: " + wordList);
		return wordList;
	}
	public void refresh(int level){
		wordList=readWordFile(level);
		setLevel(level);
		newWord();
		lbPhrase.setText(activePhrase);
		getImage();
		lblImage.setIcon(new ImageIcon(img));
		lbTryAgain.setVisible(false);
		lbTryAgain.setText(winOrLose);
 		lbAnswer.setVisible(false);
 		lbAnswer.setText("The Correct Spelling is " + realAnswer);
 		btnRestart.setVisible(false);
 		btnRestart.setEnabled(false);
 		btnNext.setVisible(true);
 		btnNext.setEnabled(false);
	}
	private int getPercentCorrect(){
		double c = correct;
		double w = wrong;
		return (int)((c/(c+w))*100);
	}
	private void restart(){
		wordList=readWordFile(level);
		newWord();
		lbPhrase.setText(activePhrase);
 		winOrLose=null;
 		lbTryAgain.setVisible(false);
 		lbAnswer.setVisible(false);
 		btnNext.setEnabled(false);
 		btnNext.setVisible(true);
 		btnRestart.setVisible(false);
 		lbPhrase.setText(activePhrase);
 		input.setText("");
 		getImage();
		lblImage.setIcon(new ImageIcon(img));
	}
	private void getImage(){
		imgPath="img/words/"+activeImage;
		//System.out.println(activeImage + "\nimg/words/"+activeImage + "\n" +imgPath);
		try {
			img = ImageIO.read(new File(imgPath));
		} catch (IOException e) {
			System.out.println("!!!No file for this word!!!");
			//e.printStackTrace();
			try {
				img = ImageIO.read(new File("img/placeholder.jpg"));
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(getParent(), "The placeholder image could not be loaded. Check to make sure " +
						"it is in /img/ or redownload the program.", "Critical Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}


