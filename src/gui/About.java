package gui;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;


@SuppressWarnings("serial")
public class About extends JFrame {
	private JPanel contentPane;
	public About(){
		setTitle("About");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 250, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblText = new JLabel("");
		lblText.setIcon(new ImageIcon("img/logoabout.png"));
		contentPane.setLayout(null);
		lblText.setBounds(10, 5, 224, 199);
		contentPane.add(lblText);
		
		JLabel lblPalsg = new JLabel("<html>PALS-Spelling@bitbukket<br /><br /><br />Licenced under the Apache 2.0 Licence<html>");
		lblPalsg.setBounds(43, 234, 158, 83);
		lblPalsg.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblPalsg);
		
		
	}
}
